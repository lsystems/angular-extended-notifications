shell   = require 'shelljs'
SemVer  = require('semver').SemVer

module.exports = (grunt) ->
  grunt.loadTasks file for file in grunt.file.expand 'node_modules/grunt-*/tasks'
  pkg = grunt.file.readJSON 'package.json'
  bannerDate = if grunt.template.today('yyyy') isnt '2014' then ('-' + grunt.template.today('yyyy')) else ''
  banner = [
    '/*!'
    ' * <%= pkg.name %> v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>'
    ' * (c) 2014' + bannerDate + ' L.systems SARL, Etienne Folio, Quentin Raynaud'
    ' * https://bitbucket.org/lsystems/angular-extended-notifications'
    ' * License: MIT'
    ' */'
  ].join('\n') + '\n'

  grunt.initConfig
    pkg: pkg

    license: grunt.file.read 'LICENSE'

    jshint:
      options:
        reporter: require('jshint-stylish')
      client:
        src: 'src/**/*.js'
        options:
          jshintrc: '.jshintrc'
          verbose: true

    jscs:
      options:
        force: true,
        config: '.jscsrc'
      all:
        src: [
          'src/**/*.js',
          '!src/webkitNotifications-shim.js'
          'tests/**/*.js'
        ]

    clean:
      options:
        force: true
      dist: 'dist/**/*'

    mochaProtractor:
      options:
        browsers: ['Chrome', 'Firefox']
      files: ['test/*.js']

    bumpup:
      files: ['package.json', 'bower.json']
      options:
        updateProps:
          pkg: 'package.json'

    copy:
      dist:
        files: [{
          src: 'src/webkitNotifications-shim.js'
          dest: 'dist/webkitNotifications-shim.js'
        }, {
          expand: true,
          src: 'templates/**/*'
          dest: 'dist/'
        }]

    concat:
      options:
        banner: banner
      dist:
        src: 'src/angular-extended-notifications.js'
        dest: 'dist/angular-extended-notifications.js'

    uglify:
      options:
        banner: banner
      lib:
        src: ['dist/angular-extended-notifications.js']
        dest: 'dist/angular-extended-notifications.min.js'
      shim:
        src: ['dist/webkitNotifications-shim.js']
        dest: 'dist/webkitNotifications-shim.min.js'

    karma:
      options:
        configFile: 'tests/karma.conf.coffee'
        browsers: [
          'PhantomJS',
          'Chrome',
          'Firefox'
        ]
        singleRun: true
      units:
        options:
          files: [
            'bower_components/angular/angular.js',
            'bower_components/angular-mocks/angular-mocks.js'
            'dist/angular-extended-notifications.js'
            'tests/units/**/*.js'
          ]
      functionals:
        options:
          files: [
            'bower_components/angular/angular.js',
            'bower_components/angular-mocks/angular-mocks.js'
            'dist/angular-extended-notifications.js'
            'tests/functionals/**/*.js'
          ]

  run = (cmd, msg) ->
    cmd = cmd.join('&&') if Array.isArray cmd
    res = shell.exec cmd
    grunt.log.writeln[!res.code ? 'ok' : 'ko'] msg if msg
    !res.code

  grunt.registerTask 'build', ['copy', 'concat', 'uglify']

  grunt.registerTask 'test', ['jshint', 'jscs', 'build', 'karma']

  grunt.registerTask 'default', ['clean', 'build']

  grunt.registerTask 'release', (type, step) ->
    type = type || 'patch'
    step = step || 'start'

    switch step
      when 'start'
        grunt.log.writeln 'Clean'
        grunt.task.run ['clean', 'release:' + type + ':prepare']

      when 'prepare'
        grunt.log.writeln 'Build & test & bump version…'

        run [
          'rm -fr dist && mkdir dist'
          'cd dist && git init && cd ..'
          'cp .git/config dist/.git/'
          'cp bower.json README.md dist/'
        ]

        tasks = [
          'jshint'
          'jscs'
          'build'
          # 'test'
        ]

        if type isnt 'prerelease'
          tasks.unshift 'bumpup:' + type

        tasks.push 'release:' + type + ':commit'

        grunt.task.run tasks

      when 'commit'
        grunt.log.writeln 'Commit & push release…'

        newVer = grunt.config 'pkg.version'
        run [
          'cd dist'
          'git add -A'
          'git commit -m "Release v' + newVer + '" --no-verify'
          'git tag "v' + newVer + '"'
          'git push origin --tags -f'
        ]

        grunt.log.writeln 'Publish on NPM…'
        run [
          'rm dist/bower.json'
          'cp package.json dist/'
          'cd dist'
          'npm publish'
        ]

        version = new SemVer(grunt.config('pkg.version'))
        if type isnt 'prerelease'
          version.inc('patch')
          version.prerelease = ['pre']
        else
          version.inc('prerelease')

        grunt.log.writeln 'Bump new dev version…'
        grunt.task.run ['bumpup:' + version.format(), 'release:' + type + ':end']

      when 'end'
        run [
          'git add -A'
          'git commit -m "Bump version to v' + grunt.config('pkg.version') + ' in master" --no-verify'
          'git push origin master'
        ]
